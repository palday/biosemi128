subjnr = 16
subj = strcat('SAM_',num2str(subjnr))

% load Biosemi and perform basic processing
EEG = pop_fileio(strcat(subj,'.bdf') );
% load channel positions
EEG = pop_chanedit(EEG, 'load',{'biosemi128.ced' 'filetype' 'autodetect'});
% downsample to 500 Hz 
EEG = pop_resample(EEG, 500);
% average reference but exclude non EEG measurements
EEG = pop_reref( EEG, [], 'exclude',[137:144]);
% save new minimally processed file
EEG = pop_saveset(EEG, 'filename',strcat(subj,'_500Hz','.set'));


% remove GSR and other non EEG measurements 
EEG = pop_select( EEG,'nochannel',{'GSR1' 'GSR2' 'Erg1' 'Erg2' 'Resp' 'Plet' 'Temp' 'Status'});

EEG = pop_cleanline(EEG, 'bandwidth',2,'chanlist', [1:EEG.nbchan] ,'computepower',0,'linefreqs',50,'normSpectrum',0,'p',0.01,'pad',2,'plotfigures',0,'scanforlines',1,'sigtype','Channels','tau',100,'verb',1,'winsize',4,'winstep',1);
EEG = pop_saveset(EEG, 'filename',strcat(subj,'_500Hz_cleanline','.set'));

EEGpreICA = EEG;

% resample to save memory and speed up ICA
EEG = pop_resample(EEG, 100);

% horrible highpass filter for stationarity
% (ICA works a lot better when data is stationary!)
EEG = pop_eegfiltnew(EEG, 1, 0);

% Adaptive Mixture ICA
% store as TMP
[TMP.icaweights, TMP.icasphere, TMP.mods] = runamica12( EEG.data(:,:), 'do_reject',1, 'numrej',5,'max_iter',1000,'max_threads',amicaprocs,'numprocs',amicaprocs); 
% here ends the work on the flatlined file; we now have an
% ICA decomposition in TMP

% now we preprocess the original more modestly,
% add the ICA we just conducted, and save the actual file we'll
% later actually analyse

EEG = eeg_checkset(EEGpreICA);

% nice filter (0.1 hz)
EEG = pop_eegfiltnew(EEG, 0.1, 0);

% import AMICA decomp from TMP
mods = TMP.mods;
EEG.icasphere = mods.S(1:mods.num_pcs,:);
EEG.icaweights = mods.W(:,:,1);
EEG.icawinv = mods.A(:,:,1);

EEG = eeg_checkset(EEG);

% automatically detect artifactual ICs and store in EEG.artcomps
%[EEG.artcomps, info] = MARA(EEG);
%EEG = eeg_checkset(EEG);

EEG = pop_saveset(EEG, 'filename',strcat(subj,'_500Hz_cleanline_AMICA','.set'));
